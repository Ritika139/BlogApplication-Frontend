import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AppserviceService} from '../appservice.service';
import {UserserviceService} from '../userservice.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  blogs: any;
  userdata: any;
category;
  title;
access = 'public';
  constructor(private router: Router, private service: AppserviceService, private http: UserserviceService) {
  }

  ngOnInit() {
    if (!this.service.checklogin()) {
      this.router.navigate(['login']);
    }
    this.http.getuserdetails().subscribe((data) => {
      this.userdata = data;
      this.http.showfollowblog(this.userdata.userid).subscribe((data1) => {
        this.blogs = data1;
      });
    });
  }
  search() {
this.http.searchblog(this.title, this.access).subscribe((data) => {
  this.blogs = data;
});
}

  selectChangeHandler(event: any) {
    this.category = event.target.value;
    console.log(this.category);
    if (this.category == 'Select') {
      this.http.showfollowblog(this.userdata.userid).subscribe((data1) => {
        this.blogs = data1;
      });
    } else {
      this.http.findbycategory(this.category, this.access).subscribe((data) => {
        this.blogs = data;
      });
    }
  }
  logout() {
    this.service.isLoggedIn(false);
    this.router.navigate(['login']);
  }
  goTobloggerprofile(userid: number) {
    this.router.navigate(['/bloggerprofile'], { queryParams: { userid}});
  }
 }

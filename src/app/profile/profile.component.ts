import { Component, OnInit } from '@angular/core';
import {AppserviceService} from '../appservice.service';
import {Router} from '@angular/router';
import {UserserviceService} from '../userservice.service';
import {AuthenticateserviceService} from '../authenticateservice.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  userdata: any;
  disp = false;
blogs;
  // tslint:disable-next-line:max-line-length
  constructor(private service: AppserviceService, private router: Router, private http: UserserviceService, private service1: AuthenticateserviceService) {
  }
  // tslint:disable-next-line:max-line-length
  private list: {blogid: 0, description: '', access: '', category: '', date: '', image: '', title: '' , user };
blog;
folower: any;
  ngOnInit() {
    if (!this.service.checklogin()) {
      this.router.navigate(['login']);
    }
    this.http.getuserdetails().subscribe((data) => {
      this.userdata = data;
      this.http.showmyblog().subscribe((data1) => {
        this.blogs = data1;
        this.http.showmyfollower().subscribe((data2) => {
          this.folower = data2;
          console.log(this.folower);
        });
      });
    });


  }
  updateprofile() {
    this.http.updateprofile(this.userdata, this.userdata.userid).subscribe((data9) => {
      alert('Profile Updated! LOGIN AGAIN');
      this.service1.logOut();
      this.router.navigate(['login']);
    });
  }
  updatethisblog(blogid) {
    this.http.updateblog(this.list, blogid).subscribe((data5) => {
  this.ngOnInit();
    });
  }
  findbyid(blogid) {
    this.http.findbyid(blogid).subscribe((data1) => {
      this.blog = data1;
      this.list = this.blog;
      this.disp = true;
    });
  }
  kr() {
    this.disp = true;
  }
deleteblog(blogid) {
    this.http.deleteblog(blogid).subscribe((data) => {
      this.ngOnInit();
      this.disp = true;
    });
}
}

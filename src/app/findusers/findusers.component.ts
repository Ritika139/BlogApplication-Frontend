import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AppserviceService} from '../appservice.service';
import {UserserviceService} from '../userservice.service';

@Component({
  selector: 'app-findusers',
  templateUrl: './findusers.component.html',
  styleUrls: ['./findusers.component.scss']
})
export class FindusersComponent implements OnInit {
users;
unfollow;
follow;
userdata: any;
setcurrent;
  constructor(private router: Router, private service: AppserviceService, private http: UserserviceService) { }

  ngOnInit() {
    if (!this.service.checklogin()) {
      this.router.navigate(['login']);
    }
    this.http.getusers().subscribe((data) => {
      this.users = data;
    });
    this.http.getuserdetails().subscribe((data) => {
      this.userdata = data;
  });
  }
  addfriend(id) {
    this.http.addfollower(id).subscribe((data) => {
    });
  }
  goTobloggerprofile(userid: number) {
    this.router.navigate(['/bloggerprofile'], { queryParams: { userid}});
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { HomePageComponent } from './home-page/home-page.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { FrontPageComponent } from './front-page/front-page.component';
import { SearchFollowerComponent } from './search-follower/search-follower.component';
import {FormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { ProfileComponent } from './profile/profile.component';
import { BloggerprofileComponent } from './bloggerprofile/bloggerprofile.component';
import { FindusersComponent } from './findusers/findusers.component';
import { AddblogComponent } from './addblog/addblog.component';
import { BlogdetailsComponent } from './blogdetails/blogdetails.component';

@NgModule({
  declarations: [
    AppComponent,
    UpdateProfileComponent,
    HomePageComponent,
    NavBarComponent,
    LoginComponent,
    SignUpComponent,
    FrontPageComponent,
    SearchFollowerComponent,
    ProfileComponent,
    BloggerprofileComponent,
    FindusersComponent,
    AddblogComponent,
    BlogdetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import {AppserviceService} from '../appservice.service';
import {Router} from '@angular/router';
import {AuthenticateserviceService} from '../authenticateservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username;
  password;
  constructor(private service: AppserviceService, private router: Router, private authservice: AuthenticateserviceService) { }

  ngOnInit() {
  }
  login() {
    this.authservice.authenticate(this.username, this.password).subscribe(data => {
        this.service.isLoggedIn(true);
        this.router.navigate(['homepage']);
      },
      (error) => {
        alert('Invalid Credential');
      }
    );
  }
  logout() {
    this.service.isLoggedIn(false);
  }
}

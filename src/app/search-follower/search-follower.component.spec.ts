import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchFollowerComponent } from './search-follower.component';

describe('SearchFollowerComponent', () => {
  let component: SearchFollowerComponent;
  let fixture: ComponentFixture<SearchFollowerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchFollowerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFollowerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

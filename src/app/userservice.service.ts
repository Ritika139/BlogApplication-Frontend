import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {

  constructor(private httpClient: HttpClient) {
  }
  getusers() {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.httpClient.get('http://localhost:2020/users/get1', {headers});
  }
  updateprofile( data, id) {
    console.log(data);
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.httpClient.put('http://localhost:2020/users/updateuser/' + id , data , {headers});
  }
  getuserdetails() {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.httpClient.get('http://localhost:2020/users/getuserdetails', {headers});
  }
  getallblogs() {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.httpClient.get('http://localhost:2020/api/showblogs', {headers});
  }
  addnewblog(data) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.httpClient.post('http://localhost:2020/api/addnewblog', data, {headers});
  }
  findbyid(id) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.httpClient.get('http://localhost:2020/api/find-by-id/' + id, {headers});
  }
  showmyblog() {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.httpClient.get('http://localhost:2020/api/showmyblog/recieve', {headers});
  }
  addfollower(userid) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.httpClient.post('http://localhost:2020/follow/addfollower/' + userid, userid, {headers});
  }
  updateblog( data, id) {
    console.log(data);
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.httpClient.put('http://localhost:2020/api/updateblog/' + id , data , {headers});
  }
  deleteblog(id) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.httpClient.delete('http://localhost:2020/api/delete-blog/' + id, {headers});
  }
  showuserblog(id) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.httpClient.get('http://localhost:2020/api/showuserblog/' + id, {headers});
  }
  findbyuserid(userid) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.httpClient.get('http://localhost:2020/users/find-by-id/' + userid, {headers});
  }
  showmyfollower() {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.httpClient.get('http://localhost:2020/follow/showmyfollower', {headers});
  }
  showfollowblog(id) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.httpClient.get('http://localhost:2020/api/showfollowblog/' + id, {headers});
  }
  searchblog(type1, type2) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.httpClient.get('http://localhost:2020/api/find-blog/' + type1 + '/' + type2, { headers});
  }

  findbycategory(number1: any, number2: any) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: 'Basic ' + token});
    return this.httpClient.get('http://localhost:2020/api/find-by-category/' + number1 + '/' + number2 , { headers});
  }

  // findbytitleandcategory(number1: any, number2: any, title: any) {
  //
  // }
}

import { Component, OnInit } from '@angular/core';
import {UserserviceService} from '../userservice.service';

@Component({
  selector: 'app-addblog',
  templateUrl: './addblog.component.html',
  styleUrls: ['./addblog.component.scss']
})
export class AddblogComponent implements OnInit {

title;
description;
category;
image;
setblog;
  constructor(private http: UserserviceService) { }

  ngOnInit() {
  }
addblog() {
  const data = {
    title: this.title,
    image: this.image,
    description: this.description,
    category: this.category,
  };
  console.log(data);
  this.setblog = true;
  this.http.addnewblog(data).subscribe((data5) => {
  });
}

}

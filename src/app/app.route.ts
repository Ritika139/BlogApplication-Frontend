import {Routes} from '@angular/router';
import {FrontPageComponent} from './front-page/front-page.component';
import {LoginComponent} from './login/login.component';
import {SignUpComponent} from './sign-up/sign-up.component';
import {HomePageComponent} from './home-page/home-page.component';
import {ProfileComponent} from './profile/profile.component';
import {BloggerprofileComponent} from './bloggerprofile/bloggerprofile.component';
import {FindusersComponent} from './findusers/findusers.component';
import {AddblogComponent} from './addblog/addblog.component';
import {BlogdetailsComponent} from './blogdetails/blogdetails.component';

export const MAIN_ROUTES: Routes = [
  {
    path : '' , redirectTo : 'login', pathMatch : 'full'
  },
  {
    path: 'frontpage',
    component: FrontPageComponent,
  },
  {
    path: 'blogdetails',
    component: BlogdetailsComponent,
  },
  {
    path: 'homepage',
    component: HomePageComponent,
  },
  {
    path: 'addnewblog',
    component: AddblogComponent,
  },
  {
    path: 'finduser',
    component: FindusersComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'bloggerprofile',
    component: BloggerprofileComponent,
  },
  {
    path: 'myprofile',
    component: ProfileComponent
  },
  {
    path: 'signup',
    component: SignUpComponent,
  }
];

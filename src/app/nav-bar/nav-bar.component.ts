import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AppserviceService} from '../appservice.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  constructor(private router: Router, private service: AppserviceService) { }

  ngOnInit() {
    if (!this.service.checklogin()) {
      this.router.navigate(['login']);
    }
  }
  logout() {
    this.service.isLoggedIn(false);
    this.router.navigate(['login']);
  }
}

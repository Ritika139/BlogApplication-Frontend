import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BloggerprofileComponent } from './bloggerprofile.component';

describe('BloggerprofileComponent', () => {
  let component: BloggerprofileComponent;
  let fixture: ComponentFixture<BloggerprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BloggerprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BloggerprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

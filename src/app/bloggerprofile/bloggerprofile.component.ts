import { Component, OnInit } from '@angular/core';
import {UserserviceService} from '../userservice.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-bloggerprofile',
  templateUrl: './bloggerprofile.component.html',
  styleUrls: ['./bloggerprofile.component.scss']
})
export class BloggerprofileComponent implements OnInit {

  constructor(private http: UserserviceService, private activatedroute: ActivatedRoute) { }
userid;
  noblog;
  showb;
  userdata: any = false;
  blogs: any = false;
  ngOnInit() {
    this.activatedroute.queryParams.subscribe((params) => {
      this.userid = params.userid;
      this.http.findbyuserid(this.userid).subscribe((data) => {
        this.userdata = data;
        this.http.showuserblog(this.userid).subscribe((data) =>{
          this.blogs = data;
          if(this.blogs.length == 0)
          this.noblog = true;
        });
      });
      });
  }
}
